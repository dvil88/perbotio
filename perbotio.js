const puppeteer = require('puppeteer');
const path = require('path');
const logger = require('./libs/logger.js');

class perbotio{
	constructor(bot, headless) {
		this.bot = bot;
		this.headless = headless;
	}
	
	async run(){
		try{
			// logger.debug(path.resolve('./bots/' + this.bot + '.js'));
			var botName = require(path.resolve('./bots/' + this.bot + '.js'));
		} catch(error){
			logger.warn(this.bot,'::','Bot not found');
			logger.error(this.bot,'::',error);
			return;
		}

		await puppeteer.launch({
			args: ["--no-sandbox", "--start-maximized"],
			ignoreHTTPSErrors: true,
			headless: this.headless,
			defaultViewport: null,
			userDataDir: './chrome-user-data/',
			pipe: true,
		}).then(async browser =>{
			logger.debug(this.bot,'::','init');

			var pages = await browser.pages();
			var page = pages[0];

			var robot = new botName();
			if (this.monthProcess >= 0) {
				robot.setMonth(this.monthProcess);
			}

			page.on('close', () => {
				logger.debug(this.bot,'::','page closed');
			});
			
			var scrapper = await robot.run(browser, page);

			if( scrapper.error === true ){
				logger.error(this.bot,'::',scrapper.errorDescription);
			} else {
				logger.info(this.bot,'::','Bot finished its job');
			}
			
			return browser;
		}).then(async browser => {
			logger.debug('close browser');
			await browser.close();
		});
	}

	async setMonth(monthProcess){
		this.monthProcess = monthProcess;
	}
}
module.exports = perbotio;
