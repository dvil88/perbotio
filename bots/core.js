const logger = require('../libs/logger.js');
const c = require('../libs/c.js');

const Config = require('conf');
const dateFormat = require('dateformat');
const prompt = require('prompt-sync')({
    sigint: false
});
const times = require('../conf/times.js');


// Load config set
const config = new Config();
const configData = config.get();


var _this;

class core {
	constructor(){
		this.pageSniffed = false;
		this.monthProcess = (new Date).getMonth();
	}

	setMonth(monthProcess){
		this.monthProcess = monthProcess;
		console.log('setted', this.monthProcess)
	}

	async run(browser, page){
		this.browser = browser;
		this.page = page;

		_this = this;

		this.page.on('response', this.response);

		this.userConfig = configData.personioUser;

		if( !this.userConfig ){
			logger.error('Error, you have to configure the Personio user details');
			do{
				var user = prompt(' * Personio user: ');
			} while( user == null );
			var pass = prompt(' * Personio password: ', {echo: '*'});

			var conf = {
				user: user,
				pass: c.e(pass),
			};
			this.userConfig = conf;
			config.set({personioUser: conf});
		}

		logger.info('Running on user \'' + this.userConfig.user + '\'');


		var working = await this.page.goto('https://clicars.personio.de/', {
			waitUntil: 'networkidle2',
		}).catch(() => {
			return false;
		});

		// Check login
		if( await this.page.$('input#email').catch(() => { }) !== null ){
			await this.doLogin();
		}

		return await this.fillAttendance();
	}

	async doLogin(){
		this.userConfig.pass = c.d(this.userConfig.pass);
		
		var userInput = await this.page.waitForSelector('input#email', {visible: true}).catch(() => { return false; });
		if( !userInput ){ return this.error('No user input'); }

		var passInput = await this.page.waitForSelector('input#password', {visible: true}).catch(() => { return false; });
		if( !passInput ){ return this.error('No password input'); }

		// Fill user and password
		await this.page.focus('input#email');
		await this.page.type('input#email', this.userConfig.user, {delay: 100});
		await this.page.focus('input#password');
		await this.page.type('input#password', this.userConfig.pass, {delay: 100});

		// Login button
		var loginButton = await this.page.waitForSelector('button[type="submit"]', {visible: true}).catch(() => { return false; });
		if( !loginButton ){ return this.error('No login button'); }

		await this.page.click('button[type="submit"]');

		// check token
		const authToken = await Promise.race([
			this.page.waitForSelector('input[name="token"]', { timeout: 15000, visible: true }).then(() => { return true; }).catch(),
			this.page.waitForSelector('[data-test-id="attendance-time-clock"]', { timeout: 15000, visible: true }).then(() => { return false; }).catch(),
		]);

		if( authToken ){
			var token = prompt(' Auth token sent to your email: ');
			await this.page.focus('input[name="token"]');
			await this.page.type('input[name="token"]', token, {delay: 100});

			await this.page.click('button[type="submit"]');
		}
	}

	async fillAttendance(){
		var sidebarPill = await this.page.waitForSelector('[data-test-id="navsidebar-profile"]', {visible: true}).catch(() => { return false; });
		if( !sidebarPill ){ return this.error('No sidebar pill'); }

		await this.page.hover('[data-test-id="navsidebar-profile"]');

		var timeSheetLink = await this.page.waitForSelector('a[data-test-id="navsidebar-sub-myAttendance"]', {visible: true}).catch(() => { return false; });
		if( !timeSheetLink ){ return this.error('No timesheet link'); }

		await this.page.click('a[data-test-id="navsidebar-sub-myAttendance"]');

		var calendar = await this.page.waitForSelector('div[class*="AttendanceCalendar"]', {visible: true}).catch(() => { return false; });
		if( !calendar ){ return this.error('No calendar'); }

		await this.page.waitForTimeout(2000);
		
		var weekDates = await this.getWeekDates();
		var emptyDates = [];
		for( let date of weekDates ){
			if( !await this.page.$('[data-test-id="day_' + dateFormat(date, 'yyyy-mm-dd') + '"] span[data-test-id="day-summary"]') ){
				emptyDates.push(date);
			}
		}

		return await this.processAttendance(emptyDates)
	}

	async processAttendance(dates){
		if( !dates.length ){
			return this.error('No dates to fill');
		}

		for( const dateOb of dates ){
			let date = dateFormat(dateOb, 'yyyy-mm-dd');
			let schedule = dateFormat(dateOb, 'm') != '7' && dateFormat(dateOb, 'm') != '8' ? times.normal : times.intensive;


			// Check if it's free day
			if( await this.page.$('[data-test-id="day_' + date + '"] div[class*="AbsenceTicker"]') ){
				logger.debug('Free day, skip', date);
				continue;
			}


			var addButton = await this.page.waitForSelector('[data-test-id="calendar-timeframe-add_' + date + '"][class*="TimeFrameAdder"]', {visible: true}).catch(() => { return false; });
			if( !addButton ){ return this.error('No add button'); }
			var clicked = await this.page.click('[data-test-id="calendar-timeframe-add_' + date + '"][class*="TimeFrameAdder"]').catch(() => false);
			if (!clicked) {
				await this.page.evaluate((date) => {
					document.querySelector('[data-test-id="calendar-timeframe-add_' + date + '"][class*="TimeFrameAdder"]').click();
				}, date);
			}

			var input = await this.page.waitForSelector('input[data-test-id="timerange-start"]', {visible: true}).catch(() => { return false; });
			if( !input ){ return this.error('No input'); }

			// Work
			await this.page.focus('[data-test-id="work-entry"] input[data-test-id="timerange-start"]');
			await this.page.waitForTimeout(1000);
			await this.page.type('[data-test-id="work-entry"] input[data-test-id="timerange-start"]', schedule.work.from, {delay: 100});

			await this.page.focus('[data-test-id="work-entry"] input[data-test-id="timerange-end"]');
			await this.page.type('[data-test-id="work-entry"] input[data-test-id="timerange-end"]', schedule.work.to, {delay: 100});

			// Project
			var selector = await this.page.waitForSelector('div[data-action-name="day-entry-field-projects"]', {visible: true, timeout: 500}).catch(() => { return false; });
			if( !selector ){
				await this.page.click('div[data-test-id="day-entry-expand-details"] > div > button');
				await this.page.waitForSelector('div[data-action-name="day-entry-field-projects"]', {visible: true}).catch(() => {});
			}

			await this.page.click('div[data-test-id="day-entry-field-projects"] button');

			await this.page.evaluate(() => {
				var elements = document.querySelectorAll('div[data-action-name="day-entry-field-projects"] ul li');
				for( var element of elements ){
					if( element.textContent == 'Teletrabajo' ){
						element.click();
						break;
					}
				}
			});


			// Rest
			if( schedule.rest ){
				await this.page.focus('[data-test-id="break-entry"] input[data-test-id="timerange-start"]');
				await this.page.type('[data-test-id="break-entry"] input[data-test-id="timerange-start"]', schedule.rest.from, {delay: 100});
				
				await this.page.focus('[data-test-id="break-entry"] input[data-test-id="timerange-end"]');
				await this.page.type('[data-test-id="break-entry"] input[data-test-id="timerange-end"]', schedule.rest.to, {delay: 100});
			}

			await this.page.waitForTimeout(500);

			await this.page.click('button[data-test-id="day-entry-save"]');
			// await this.page.click('button[data-test-id="timeframe-details-edit-close"]');

			logger.info('Day filled:', date);

			await this.waitFor(function(){ return _this.pageSniffed; }, 5000);
			_this.pageSniffed = false;
			await this.page.waitForTimeout(1000);
		}

		return { error: false };
	}

	async error(errorDescription){
		// logger.info(this.botName,'::','close page');
		// await this.page.close();
		return { error: true, errorDescription: errorDescription };
	}

	async response(response){
		if( response.url().match(/\/attendances\/days\//) && response.request().method() === 'PUT' ){
			// logger.debug('RESPONSE',response.url());
			response.text().then(function(textBody) {
				// console.log('textBody',textBody);
				_this.pageSniffed = true;
			});
		}
	}

	async waitFor(testFx, timeOutMillis) {
		var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000,
		start = new Date().getTime(),
		condition = false;
		do{
			if( (new Date().getTime() - start < maxtimeOutMillis) && !condition ){
				// If not time-out yet and condition not yet fulfilled
				condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
			} else {
				if( !condition ){
					// If condition still not fulfilled (timeout but condition is 'false')
					// logger.debug(this.botName,'::', "'waitFor()' timeout " + timeOutMillis);
					return false;
				} else {
					// Condition fulfilled (timeout and/or condition is 'true')
					// logger.debug(this.botName,'::', "'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
					return true;
				}
			}
			await this.page.waitForTimeout(250);
		} while( (new Date().getTime() - start < maxtimeOutMillis) );
		return false;
	}
}

module.exports = core;