const fs = require('fs');
const path = require('path');
const ipcRenderer = require('electron').ipcRenderer; 
const Config = require('conf');
const config = new Config();
const c = require('../libs/c.js');


var app = {
	init: function(){
		app.loadConfig();

		ipcRenderer.on('finishedScraper', function(event, scraper){
			app.enableButtons(scraper)
		});
	},
	saveConfig: function(e){
		e.preventDefault();

		var conf = {
			user: document.querySelector('#personioUser_user').value,
			pass: c.e(document.querySelector('#personioUser_pass').value),
		};
		config.set({personioUser: conf});
	},
	loadConfig: function(){
		this.configData = config.get();
	},
	d: function(s){
		return c.d(s);
	},
	sendCommand: function(command, data){
		ipcRenderer.send(command, data);
	},
	runScraper: function(e){
		var scraper = e.target.dataset.scraper;
		var headless = e.target.dataset?.headless !== 'false';
		var data = { scraper: scraper, headless: headless };

		if (scraper === 'fixMonth') {
			data.monthProcess = document.getElementById('fixMonthSelect').value;
		}

		app.disableButtons(scraper);

		app.sendCommand('runScraper', JSON.stringify(data));
	},
	disableButtons: function(scraper){
		var scraperButtons = [].slice.call(document.querySelectorAll('.' + scraper + 'Buttons button, .' + scraper + 'Buttons select'));
		scraperButtons.map(function (buttonEl) {
			buttonEl.disabled = 'disabled';
			if (buttonEl.classList.contains('runScraper') ){
				buttonEl.innerText = 'Running';
			}
		});
	},
	enableButtons: function(scraper){
		var scraperButtons = [].slice.call(document.querySelectorAll('.' + scraper + 'Buttons button, .' + scraper + 'Buttons select'));
		scraperButtons.map(function (buttonEl) {
			buttonEl.disabled = false;
			if (buttonEl.classList.contains('runScraper') ){
				buttonEl.innerText = 'Run';
			}
		});
	}
}
app.init();