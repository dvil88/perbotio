const times = {
	normal: {
		work: { from: '09:00', to: '18:00' },
		rest: { from: '14:00', to: '15:00' }
	},
	intensive: {
		work: { from: '08:00', to: '15:00'}
	}
};

module.exports = times;